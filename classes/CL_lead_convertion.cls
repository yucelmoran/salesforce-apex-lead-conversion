public class CL_lead_convertion {
	
	public void convertLeadProcess(Lead[] candidates){
		Database.LeadConvert leadConversion = new database.LeadConvert();
		
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		

		for(Lead candidate:candidates){			
			//Here we need a condition to filter records previuos to the convertion.			
          	leadConversion.setLeadId(candidate.Id);
      		leadConversion.setDoNotCreateOpportunity(true);//***Uncomment this Line if we want to create opportunities	
      		leadConversion.setConvertedStatus(convertStatus.MasterLabel);          	
		}
		//Executing convertion of the Lead.
		Database.LeadConvertResult leadConvertionResults = Database.convertLead(leadConversion);
		enhanceAccountFields(leadConvertionResults.accountId);
		
	}
	
	/*
		@Description:	Allow users enhance Account fields with the last conversion
						of the lead.
		@Author:		Edgar Yucel Moran Sanchez
		@Date:			March 18, 2014
		@Version:		1.0	
	*/
	public void enhanceAccountFields(Id convertedAccountId){
		Map<String,Id> mapRecordTypes=new Map<String,Id>();
		
		for(RecordType rt:[Select Id,Name from RecordType]){
			mapRecordTypes.put(rt.Name,rt.Id);
		}
		
		//Here we need to include all fields we want to add from lead new
		Account acc =[Select Id,Name,Description from Account where Id=:convertedAccountId];
		acc.Description = 'This is a test of enhanced conversion lead';
		if(mapRecordTypes.get('Leads')!=null){
			acc.RecordTypeId = mapRecordTypes.get('Leads');
		}
		update acc;				
		
		
	}
	
	/*
		@Description:	Allow users enhance Contact fields with the last conversion
						of the lead.
		@Author:		Edgar Yucel Moran Sanchez
		@Date:			March 18, 2014
		@Version:		1.0	
	*/
	public void enhanceContactFields(Lead candidateNew,Lead candidateOld){
		
		if(candidateOld.isConverted == false && candidateNew.IsConverted ==true){
			
			//if a Account was created
			if(candidateNew.ConvertedContactId != null){
				//Here we need to include all fields we want to add from lead new
				Contact c = [Select c.Id, c.Description, c.Name From Contact c Where c.Id = :candidateNew.ConvertedContactId];
    			c.Description = 'Test from Contact enhacements lead conversion';
       			update c;			
			}
		}
	}
	
	/*
		@Description:	Allow users enhance Contact fields with the last conversion
						of the lead.
		@Author:		Edgar Yucel Moran Sanchez
		@Date:			March 18, 2014
		@Version:		1.0	
	*/
	public void enhanceOpportunityFields(Lead candidateNew,Lead candidateOld){
		
		if(candidateOld.isConverted == false && candidateNew.IsConverted ==true){
			
			//if a Account was created
			if(candidateNew.ConvertedOpportunityId != null){
				//Here we need to include all fields we want to add from lead new
				// update the converted opportunity with some text from the lead
		        Opportunity opp = [Select o.Id, o.Description from Opportunity o Where o.Id = :candidateNew.ConvertedOpportunityId];
		        opp.Description = 'Opportunity description demo from lead conversion';
		        update opp;
		 
		        // add an opportunity line item
		        OpportunityLineItem oli = new OpportunityLineItem();
		        oli.OpportunityId = opp.Id;
		        oli.Quantity = 1;
		        oli.TotalPrice = 100.00;
		        oli.PricebookEntryId = [Select p.Id From PricebookEntry p Where IsActive = true limit 1].Id;
		        insert oli;			
			}
		}
	}
}